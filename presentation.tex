\documentclass[usenames,dvipsnames,aspectratio=169]{beamer}
\title{On hyperon masses, axial charges, and form factors}

\author[shortname]{
	\texorpdfstring{\textcolor{ProcessBlue}{Nolan Miller}}{} \and 
	G.~Bradley \and
	M.~Lazarow \and
	H.~Monge-Camacho \and
	A.~Nicholson \and
	P.~Vranas \and
	A.~Walker-Loud \and
	\emph{others}
}
					  
\date{July 28, 2021}

\titlegraphic{
	\includegraphics[height=2cm]{figs/callat_logo.png}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/doe_sc_logo.pdf}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/unc_logo.png} %unc_logo
}

\usepackage{braket}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{slashed}
\usepackage{cancel}

\usetheme{default}
\usecolortheme{dove}
\setbeamercolor{frametitle}{fg=RoyalPurple,bg=Orchid!20}

\setbeamertemplate{navigation symbols}{%
	\usebeamerfont{footline}%
	\usebeamercolor[fg]{footline}%
	\hspace{1em}%
	\insertframenumber
}
\addtobeamertemplate{frametitle}{
   \let\insertframesubtitle\insertsectionhead}{}
\setbeamertemplate{background}
{\includegraphics[width=\paperwidth,keepaspectratio]{figs/background.jpg}}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

%\begin{frame}
%	\frametitle{Outline}
%	\tableofcontents
%\end{frame}

\begin{frame}
	\frametitle{Why hyperons?}

	{\color{ProcessBlue} Hyperon}: a baryon containing at least one strange quark but no heavier quarks
	
	\begin{columns}
	\begin{column}{0.5\textwidth}

	Why study hyperons?
	\begin{itemize}
		\item {\color{JungleGreen} Decays $\implies$ $V_{us}$ $\implies$  
		top-row unitarity: $|V_{ud}|^2 + | V_{us}|^2 +| V_{ub}|^2 \stackrel{?}{=} 1$}
		\item Axial charge, mass spectra important for neutron star equation of state
		\item Test heavy baryon $\chi$PT
	\end{itemize}
	\vspace{\baselineskip}

	Why the lattice?
	\begin{itemize}
		\item Hypernuclear structure harder to study experimentally
		\item Hyperons decay rapidly in the lab ($\tau < 1$ ns)
	\end{itemize}	
	\end{column}
	\begin{column}{0.4\textwidth}
		\begin{figure}
			\includegraphics[width=0.4\textheight]{./figs/baryon_octet.png}
			\includegraphics[width=0.4\textheight]{./figs/baryon_decuplet.png}
			\caption*{[Wikipedia]}
		\end{figure}
	\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Experimental determination of $|V_{us}|$?}

	Experimental results are less precise without lattice QCD input
	\begin{itemize}
		\item Leptonic/semi-leptonic $K$ decays: requires LQCD estimate of $F_K/F_\pi$ or $f^+(0)$
		\item Hyperon decays: requires estimate of axial charge, vector charge, and other form factors; {\color{ProcessBlue} new results from LHCb could make this competitive}
		\item $\tau$ hadronic decays (eg, $\tau^- \rightarrow \pi^- \nu_\tau$): LQCD not required, but there are theory problems
	\end{itemize}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{./figs/vus_sources.png}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Tension between $K_{l2}$ \& $K_{l3}$}

	\begin{columns}
		\begin{column}{0.3\textwidth}
			$K_{l2}$ ($f_{K^\pm}/f_{\pi^\pm}$):
			\begin{align*}
				& \sum_{q\in\{d, s, b\}}|V_{uq}|^2 = 0.99883(37) \\
				& \implies \text{3.2 $\sigma$ deviation}
			\end{align*}

			\vspace{\baselineskip}

			$K_{l3}$ ($f_{+}(0)$):
			\begin{align*}
				& \sum_{q\in\{d, s, b\}}|V_{uq}|^2 = 0.99794(37) \\
				& \implies \text{5.6 $\sigma$ deviation}
			\end{align*}
		\end{column}
		\begin{column}{0.7\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/vus_vs_vud_em.png}
				\caption{[FLAG, 2020]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Project goals \& lattice details}

	\begin{columns}
		\begin{column}{0.5\textwidth}
		Project Goals:
		\begin{enumerate}
		\item Determine the hyperon mass spectrum
		\item Determine axial/vector charges
		\item Test convergence of SU(2) HBXPT for hyperons
		\item Calculate hyperon-to-nucleon form factors 
		\end{enumerate}
		\end{column}

		\begin{column}{0.5\textwidth}
			\begin{table}[]
				\begin{tabular}{|l|l|}
				\hline
				Action         & \begin{tabular}[c]{@{}l@{}}Valence: Domain-wall\\ Sea: staggered\end{tabular} \\ \hline
				$m_\pi$        & 130 - 400 MeV                                                                 \\ \hline
				$a$            & 0.06 - 0.15 fm                                                                \\ \hline
				Scale setting? & Done!                                                                         \\ \hline
				\end{tabular}
			\end{table}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Previous work}

	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/bmw_hadron_spectrum.png}
			\caption*{[BMW, 2009; \href{https://arxiv.org/abs/0906.3599}{arXiv:0906.3599}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/lin_axial.png}
			\caption*{[Savanur \& Lin, 2018; \href{https://arxiv.org/abs/1901.00018}{arXiv:1901.00018}]}
		\end{figure}
		\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{$\Xi$ correlator fits}

	\begin{columns}
		\begin{column}{0.55\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/xi_a12m350.pdf}
		\end{figure}
		\end{column}
		\begin{column}{0.45\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/m_xi_m_pi.pdf}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Fit strategy: mass formulae}
	Consider the $S=2$ hyperons in the isospin limit
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{align*}
				M_\Xi^{(\chi)} = &\phantom{+}  M_\Xi^{(0)} \\
				&+ {\color{ProcessBlue} \sigma_\Xi} \Lambda_\chi \epsilon_\pi^2 \\
				&- \frac{3\pi}{2} {\color{JungleGreen} g_{\pi\Xi\Xi}^2} \Lambda_{\chi} \epsilon_\pi^3 \\
				&\qquad- {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} \Lambda_{\chi} \mathcal{F}(\epsilon_\pi, \epsilon_{\Xi\Xi^*}, \mu) \\
				&+ \frac{3}{2} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} ({\color{ProcessBlue} \sigma_\Xi} - {\color{ProcessBlue} \overline{\sigma}_\Xi} ) \Lambda_\chi \epsilon_\pi^2 
				\mathcal{J} (\epsilon_\pi, \epsilon_{\Xi\Xi^*}, \mu) \\
				&\qquad + \alpha_\Xi^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{\Xi}^{(4)} \Lambda_\chi \epsilon_\pi^4
			\end{align*}
		\end{column}
	\begin{column}{0.5\textwidth}
		\begin{align*}
			M_{\Xi^*}^{(\chi)} = &\phantom{+}  M_{\Xi^*}^{(0)} \\
			&+ {\color{ProcessBlue} \overline{\sigma}_\Xi} \Lambda_\chi \epsilon_\pi^2  \\
			&- \frac{5\pi}{6} {\color{JungleGreen} g_{\pi\Xi^*\Xi^*}^2} \Lambda_{\chi} \epsilon_\pi^3 \\
			&\qquad - \frac{1}{2} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} \Lambda_{\chi} \mathcal{F}(\epsilon_\pi, -\epsilon_{\Xi\Xi^*}, \mu) \\
			&+ \frac{3}{4} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} ({\color{ProcessBlue} \overline{\sigma}_\Xi} -{\color{ProcessBlue} \sigma_\Xi} ) \Lambda_\chi \epsilon_\pi^2 
			\mathcal{J} (\epsilon_\pi, -\epsilon_{\Xi\Xi^*}, \mu)  \\
			&\qquad + \alpha_{\Xi^*}^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{\Xi^*}^{(4)} \Lambda_\chi \epsilon_\pi^4
			\end{align*}
	\end{column}
	\end{columns}
	\vfill
	Some observations:
	\begin{itemize}
		\item Many {\color{ProcessBlue} shared LECs} between expressions $\implies$ fit simultaneously
		\item Mass fits depend on {\color{JungleGreen} axial charges} 
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Hyperon mass spectrum: $\Xi$ preliminary results}
	\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/m_xi_order_xpt.png}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{align*}
			\begin{array}{rl}
				+ 1:& \text{Taylor }\mathcal{O}(m^2_\pi) \\
				+ 1:& \text{$\chi$PT } \mathcal{O}(m^3_\pi)\\
				+ 3:& \text{Taylor } \mathcal{O}(m^4_\pi)\oplus \text{$\chi$PT } \left\{ 0, \mathcal{O}(m^3_\pi), \mathcal{O}(m^4_\pi) \right\} \\
				\hline 
				5:& {\color{RubineRed} \text{chiral choices}} \\
				\\
				\times 5:& {\color{RubineRed}  \text{chiral choices}}  \\
				\times 2:& \left\{ \mathcal{O}(a^2), \mathcal{O}(a^4)\right\}\\
				\times 2:& \text{incl./excl. strange mistuning} \\
				\times 2:& \text{Na\"ive priors or empirical priors} \\
				\hline
				40:& \text{total choices}
			\end{array}
			\end{align*}
	\end{column}
	\end{columns}
	\vfill
	\begin{equation*}
		\boxed{M_{\Xi} = 1339(17)^\text{s}(02)^\chi(05)^a(00)^\text{phys}(01)^\text{M}(??)^\text{V}}
	\end{equation*}
\end{frame}


\begin{frame}
	\frametitle{Summary \& future work}

	\begin{columns}
		\begin{column}{0.5\textwidth}
		In conclusion:
		\begin{itemize}
			\item Chiral mass and charge expressions share many LECs and would benefit from a simultaneous fit
			\item Hyperon decays provide an alternate method for extracting $|V_{us}|$
			\item Competitive if $O(1\%)$ determination of the form factors
		\end{itemize}
		\vspace{\baselineskip}

		To do:
		\begin{itemize}
			\item Add finite volume effects to mass fits
			\item (Simultaneously) fit axial charges
			\item Calculate hyperon-to-nucleon form factors
		\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/xi_ga_a15m400.png}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\section{Extra slides}
\begin{frame}
	\frametitle{Transition matrix element for $B_1 \rightarrow B_2 + l^- + \overline{\nu}_l$}

	\begin{equation*}
		T_\text{fi} = \frac{G_\text{F}}{\sqrt{2}} V_{us} 
		\left[
		\overbrace{\braket{B_2 | \overline u \gamma_\mu \gamma^5 s | B_1}}^\text{\color{ProcessBlue} axial-vector} 
		- \overbrace{\braket{B_2 | \overline u \gamma_\mu s | B_1}}^\text{\color{JungleGreen} vector} 
		\right]
		\overline l \gamma^\mu (1 - \gamma^5) \nu_l 
	\end{equation*}

	with hadronic matrix elements

	\begin{align*}
		{\color{ProcessBlue} \braket{B_2 | \overline u \gamma_\mu \gamma^5 s | B_1}}
		&= g_A(q^2) \gamma_\mu \gamma^5 
		+ \underbrace{\cancel{\frac{f_\text{T}(q^2)}{2 M} i \sigma_{\mu \nu} q^\nu \gamma^5}}_\text{\color{RubineRed} G-parity} + \frac{f_\text{P}(q^2)}{2 M} q_\mu \gamma^5 \\
		{\color{JungleGreen} \braket{B_2 | \overline u \gamma_\mu s | B_1}}
		&= g_V(q^2) \gamma_\mu + \frac{f_\text{M}(q^2)}{2 M} i \sigma_{\mu \nu} q^\nu 
		+ \overbrace{\cancel{\frac{f_\text{S}(q^2)}{2 M} q_\mu}}^\text{\color{RubineRed} CVC}
	\end{align*}

\end{frame}

\begin{frame}
	\frametitle{Empirical Bayes Method}

	Let $M = \{\Pi, f\}$ denote a model. Per Bayes's theorem:
	\begin{equation*}
		{\color{RubineRed} p(\Pi | D, f)} 
		= \frac{{\color{ProcessBlue} p(D |\Pi, f)} {\color{JungleGreen} p(\Pi | f)}}{p(D | f)}
	\end{equation*}

	Assuming a uniform distribution for ${\color{JungleGreen} p(\Pi | f)}$:
	\begin{equation*}
		\text{peak of } {\color{ProcessBlue} p(D |\Pi, f)} 
		\implies \text{peak of }  {\color{RubineRed} p(\Pi | D, f)}
	\end{equation*}
	where ${\color{ProcessBlue} p(D |\Pi, f)} $ is the (readily available) likelihood
	\vfill
	\begin{columns}
		\begin{column}{0.5\textwidth}
		\begin{itemize}
			\item Caveat: the uniformity assumption breaks down if we vary too many parameters separately or make our priors too narrow
		\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{table}[]
				\begin{tabular}{|l|c|c|}
				\hline
				$\Pi$       & ${\color{ProcessBlue} p(D| \Pi, f)}$ & ${\color{JungleGreen} p(\Pi|f)}$ \\ \hline
				$0 \pm 0.1$ & 0.27           & 0.33       \\ 
				$0 \pm 1$   & 0.54           & 0.33       \\ 
				$0 \pm 10$  & 0.19           & 0.33       \\ \hline
				\end{tabular}
				\end{table}
			\end{column}
	\end{columns}
\end{frame}

\end{document}
