# hyperon talk

Talk for Lattice 2021.

## Abstract
### On hyperon masses, axial charges, and form factors

Hyperon physics is expected to play a role in neutron stars, where the extreme neutron degeneracy pressure could push neutrons into hyperons; indeed, the composition of neutron stars contributes to the "softness" or "stiffness" of the equation of state, therefore setting limits on the possible sizes and masses of neutron stars. In this talk, I will present calculations of hyperonic observables, in particular the axial charges and masses. This work is based on a particular formulation of an SU(2) chiral perturbation theory for hyperons; determining the extent to which this effective field theory converges is instrumental in understanding the limits of its predictive power, especially since some hyperonic observables are difficult to calculate near the physical pion mass (eg, hyperon-to-nucleon form factors) and must be determined via an extrapolation to the physical point instead.

## Funding
This research was funded by a DOE SCGSR award. 