import gvar as gv
import matplotlib.pyplot as plt
import os

project_path = os.path.normpath(os.path.join(os.path.realpath(__file__), os.pardir, os.pardir))

plt.rcParams["figure.figsize"] = (10,3)
plt.rcParams["font.size"] = 16

v_us = {
    r'$\tau$ (incl./excl.)' : gv.gvar('0.2221(13)'),
    'Hyperons' : gv.gvar('0.2250(27)'),
    r'$K_{l2} \quad & \quad K_{l3}$' : gv.gvar('0.2248(07)'),
}

fig, ax = plt.subplots()

colors = ['lightcoral', 'royalblue', 'seagreen']
pm = lambda g, k : gv.mean(g) + k *gv.sdev(g)
for j, key in enumerate(v_us):
    color = colors[j]
    ax.errorbar(x=gv.mean(v_us[key]), xerr=gv.sdev(v_us[key]), y=j, yerr=0, fmt='o', mec='white', color=color, lw=10, ms=15)
    ax.axvspan(xmin=pm(v_us[key], -1), xmax=pm(v_us[key], 1), color=color, alpha=0.5)

plt.yticks(range(len(v_us)), list(v_us))
plt.xlabel(r'$|V_{us}|$')
plt.ylim(-0.5, 2.5)
plt.tight_layout()
plt.savefig(f'{project_path}/figs/vus_sources.png', transparent=True)
plt.show()